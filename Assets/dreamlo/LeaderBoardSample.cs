using UnityEngine;
using System.Collections.Generic;

public class LeaderBoardSample : MonoBehaviour {
	float startTime = 1.0f;
	float timeLeft = 0.0f;
	
	int totalScore = 0;
	string playerName = "";
    public Texture froggerTitle;
    public Texture quitGame;
    public GUIStyle SStyle;

	enum gameState {
		waiting,
		running,
		enterscore,
		leaderboard
	};
	
	gameState gs;
	
	
	// Reference to the dreamloLeaderboard prefab in the scene
	
	dreamloLeaderBoard dl;
	
	void Start () 
	{
		// get the reference here...
		this.dl = dreamloLeaderBoard.GetSceneDreamloLeaderboard();
		
		this.timeLeft = startTime;
		this.gs = gameState.waiting;
	}
	
	void Update () 
	{
		if (this.gs == gameState.running)
		{
			timeLeft = Mathf.Clamp(timeLeft - Time.deltaTime, 0, this.startTime);
			if (timeLeft == 0)
			{
				this.gs = gameState.enterscore;
			}
		}
	}
	
	void OnGUI()
	{
        GUI.DrawTexture(new Rect(10, 10, Screen.width, 200), froggerTitle, ScaleMode.ScaleToFit, true, 2);
		GUILayoutOption[] width200 = new GUILayoutOption[] {GUILayout.Width(200)};
		
		float width = 400;
		float height = 400;
		
		GUILayout.BeginArea(new Rect((Screen.width / 2) - (width / 2), (Screen.height / 2) - (height / 2), width, height));
		
		GUILayout.BeginVertical();
		if (this.gs == gameState.waiting || this.gs == gameState.running)
            this.gs = gameState.running;	
		
		if (this.gs == gameState.enterscore)
		{
			GUILayout.Label("Total Score: " + HUD.Score.ToString(), SStyle);
			GUILayout.Label("Your Name: ", SStyle);
            GUILayout.BeginHorizontal();
			this.playerName = GUILayout.TextField(this.playerName, width200);
			
			if (GUILayout.Button("Save Score"))
			{
				// add the score...
				dl.AddScore(this.playerName, (int)HUD.Score);
				
				this.gs = gameState.leaderboard;
			}
			GUILayout.EndHorizontal();
		}
		
		if (this.gs == gameState.leaderboard)
		{
			GUILayout.Label("High Scores:", SStyle);
			List<dreamloLeaderBoard.Score> scoreList = dl.ToListHighToLow();
			
			if (scoreList == null) 
			{
				GUILayout.Label("(loading...)");
			} 
			else 
			{
				foreach (dreamloLeaderBoard.Score currentScore in scoreList)
				{
					GUILayout.BeginHorizontal();
					GUILayout.Label(currentScore.playerName, SStyle, width200);
					GUILayout.Label(currentScore.score.ToString(), SStyle, width200);
					GUILayout.EndHorizontal();
				}
			}
		}
        if (GUILayout.Button("Quit"))
        {
            Application.LoadLevel("MenuGame");
        }
		GUILayout.EndVertical();
		GUILayout.EndArea();
	}
	
	
}
