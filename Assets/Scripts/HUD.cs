using UnityEngine;
using System.Collections;

public class HUD : MonoBehaviour {

    public Texture froggerTitle;
    public Texture returnGame;
    public Texture returntest;
    public Texture quitGame;
    public static uint Score;
    public static int Life;
    public GUIStyle SLStyle;

	// Use this for initialization
	void Start () {
        Life = 5;
	}
	
	// Update is called once per frame
	void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
            }
            else
            {
                Time.timeScale = 1;
            }
        }
    }

    void OnGUI()
    {
        if (Time.timeScale == 0)
        {
            GUI.DrawTexture(new Rect(10, 10, Screen.width - 20, 200), froggerTitle, ScaleMode.ScaleToFit, true, 2);
            GUI.DrawTexture(new Rect(10, 50, Screen.width - 20, Screen.height), returntest, ScaleMode.ScaleToFit, true, 2);
            if (GUI.Button(new Rect(50, 200, Screen.width - 100, 200), returnGame))
                Time.timeScale = 1;
            if (GUI.Button(new Rect(50, 400, Screen.width - 100, 200), quitGame))
                Application.LoadLevel("MenuGame");
        }
        GUI.Label(new Rect(10, 10, 200, 50), "Score : " + Score.ToString(), SLStyle);
        GUI.Label(new Rect(Screen.width - 300, 10, 20, 50), "Life : " + Life.ToString(), SLStyle);
    }
}
