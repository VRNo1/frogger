using UnityEngine;
using System.Collections;

public class EndZones : MonoBehaviour {

    public static s_final finalOne;
    public static s_final finalTwo;
    public static s_final finalThree;
    public static s_final finalFour;
    public static s_final finalFive;
    public GameObject[] zone;
    public Object start;
    public Object road;
    public Object middle;
    public Object river;
    public Object finalZone;

	// Use this for initialization
	void Start () {
        Init();
        Instantiate(finalOne.endPos);
        Instantiate(finalTwo.endPos);
        Instantiate(finalThree.endPos);
        Instantiate(finalFour.endPos);
        Instantiate(finalFive.endPos);
        Instantiate(start);
        Instantiate(road);
        Instantiate(middle);
        Instantiate(river);
        Instantiate(finalZone);
	}

    void Init()
    {
        finalOne.endPos = zone[0];
        finalTwo.endPos = zone[1];
        finalThree.endPos = zone[2];
        finalFour.endPos = zone[3];
        finalFive.endPos = zone[4];
        finalOne.IsFull = false;
        finalTwo.IsFull = false;
        finalThree.IsFull = false;
        finalFour.IsFull = false;
        finalFive.IsFull = false;
    }
	// Update is called once per frame
	void Update () {
	
	}
}

public struct s_final
{
    public GameObject endPos;
    public bool IsFull;
};