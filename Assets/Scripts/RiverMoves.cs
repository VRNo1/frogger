using UnityEngine;
using System.Collections;

public class RiverMoves : MonoBehaviour {

    public e_orientation orient;
    public float speed;
    private float posY;

	// Use this for initialization
    void Start()
    {
        if (speed == 0 || speed == 5)
            speed = 1;
        else if (Frog.currentLevel != 0 && Frog.currentLevel % 5 == 0)
            speed += 1;
        posY = transform.position.y;
    }
	
	// Update is called once per frame
	void Update () {
	    if (orient == e_orientation.LEFT)
            transform.position = new Vector3(transform.position.x - speed * Time.deltaTime, posY, 1.25f);
        else
            transform.position = new Vector3(transform.position.x + speed * Time.deltaTime, posY, 1.25f);
        if (transform.position.x < -11.5)
            transform.position = new Vector3(11.5f, posY, transform.position.z);
        else if (transform.position.x > 11.5)
            transform.position = new Vector3(-11.5f, posY, transform.position.z);
	}
}