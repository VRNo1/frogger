using UnityEngine;
using System.Collections;

public class GameOverMenu : MonoBehaviour {

    public Texture froggerTitle;
    public Texture newGame;
    public Texture leaderGame;
    public Texture quitGame;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnGUI()
    {
        GUI.DrawTexture(new Rect(10, 10, Screen.width, 200), froggerTitle, ScaleMode.ScaleToFit, true, 2);
        if (GUI.Button(new Rect(10, 200, Screen.width - 20, 200), newGame))
            Application.LoadLevel("Level");
        if (GUI.Button(new Rect(10, 400, Screen.width - 20, 200), leaderGame))
            Application.LoadLevel("LeaderBoard");
        if (GUI.Button(new Rect(10, 600, Screen.width - 20, 200), quitGame))
            Application.LoadLevel("MenuGame");

    }
}
