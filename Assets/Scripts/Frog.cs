using UnityEngine;
using System.Collections;

public class Frog : MonoBehaviour
{
    public GameObject finishFrog;
    public Vector3 posStartFrog;
    public float[] posY;
    public GameObject frogModel;
    public float t;
    private int array;
    public static int currentLevel;
    public RaycastHit hit;
    public GUIStyle TStyle;
    public float Timer;

    // Use this for initialization
    void Start()
    {
        Timer = 30;
        array = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (HUD.Life < 0)
            Application.LoadLevel("GameOver");
        if (!Physics.Raycast(transform.position, transform.forward, out hit, 100.0f))
        {
            array = 0;
            HUD.Life -= 1;
            Timer = 30;
            transform.position = posStartFrog;
        }
        if (Timer == 0)
        {
            array = 0;
            HUD.Life -= 1;
            Timer = 30;
            transform.position = posStartFrog;
        }
        if (hit.collider.CompareTag("AIRiverTree"))
            this.transform.parent = hit.collider.gameObject.transform;
        else if (hit.collider.CompareTag("AIRiverTurtle"))
            this.transform.parent = hit.collider.gameObject.transform;
        else if (hit.collider.CompareTag("AIRiver"))
        {
            array = 0;
            HUD.Life -= 1;
            Timer = 30;
            transform.position = posStartFrog;
        }
        else
            transform.parent = null;
        Timer -= Time.deltaTime;
        endLevel();
    }

    void LateUpdate()
    {
        keyDown();
    }

    void endLevel()
    {
        if (EndZones.finalOne.IsFull && EndZones.finalTwo.IsFull && EndZones.finalThree.IsFull && EndZones.finalFour.IsFull && EndZones.finalFive.IsFull)
        {
            currentLevel += 1;
            Application.LoadLevel("Level");
        }
    }

    void keyDown()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if ((array += 1) > posY.Length)
                array = posY.Length;
            HUD.Score += 10;
            transform.rotation = Quaternion.Euler(0, 0, 0);
            transform.position = new Vector3(transform.position.x, posY[array], 0);
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if ((array -= 1) < 0)
                array = 0;
            transform.rotation = Quaternion.Euler(0, 0, 180);
            transform.position = new Vector3(transform.position.x, posY[array], 0);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            transform.rotation = Quaternion.Euler(0, 0, -90);
            transform.position = new Vector3(transform.position.x + t * Time.deltaTime, posY[array], 0);
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            transform.rotation = Quaternion.Euler(0, 0, 90);
            transform.position = new Vector3(transform.position.x - t * Time.deltaTime, posY[array], 0);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("AI"))
        {
            array = 0;
            HUD.Life -= 1;
            transform.position = posStartFrog;
            Timer = 30;
        }
        if (collision.gameObject.CompareTag("Finish"))
        {
            if (collision.gameObject.name == "endOne(Clone)")
            {
                if (EndZones.finalOne.IsFull == false)
                {
                    HUD.Score += 200;
                    Instantiate(finishFrog, EndZones.finalOne.endPos.transform.position, EndZones.finalOne.endPos.transform.rotation);
                    transform.position = posStartFrog;
                    EndZones.finalOne.IsFull = true;
                    array = 0;
                    Timer = 30;
                }
                else
                {
                    array -= 1;
                    transform.position = new Vector3(transform.position.x, posY[array], transform.position.z);
                }
            }
            if (collision.gameObject.name == "endTwo(Clone)")
            {
                if (EndZones.finalTwo.IsFull == false)
                {
                    HUD.Score += 200;
                    Instantiate(finishFrog, EndZones.finalTwo.endPos.transform.position, EndZones.finalTwo.endPos.transform.rotation);
                    transform.position = posStartFrog;
                    EndZones.finalTwo.IsFull = true;
                    array = 0;
                    Timer = 30;
                }
                else
                {
                    array -= 1;
                    transform.position = new Vector3(transform.position.x, posY[array], transform.position.z);
                }
            }
            if (collision.gameObject.name == "endThree(Clone)")
            {
                if (EndZones.finalThree.IsFull == false)
                {
                    HUD.Score += 200;
                    Instantiate(finishFrog, EndZones.finalThree.endPos.transform.position, EndZones.finalThree.endPos.transform.rotation);
                    transform.position = posStartFrog;
                    EndZones.finalThree.IsFull = true;
                    array = 0;
                    Timer = 30;
                }
                else
                {
                    array -= 1;
                    transform.position = new Vector3(transform.position.x, posY[array], transform.position.z);
                }
            }
            if (collision.gameObject.name == "endFour(Clone)")
            {
                if (EndZones.finalFour.IsFull == false)
                {
                    HUD.Score += 200;
                    Instantiate(finishFrog, EndZones.finalFour.endPos.transform.position, EndZones.finalFour.endPos.transform.rotation);
                    transform.position = posStartFrog;
                    EndZones.finalFour.IsFull = true;
                    array = 0;
                    Timer = 30;
                }
                else
                {
                    array -= 1;
                    transform.position = new Vector3(transform.position.x, posY[array], transform.position.z);
                }
            }
            if (collision.gameObject.name == "endFive(Clone)")
            {
                if (EndZones.finalFive.IsFull == false)
                {
                    HUD.Score += 200;
                    Instantiate(finishFrog, EndZones.finalFive.endPos.transform.position, EndZones.finalFive.endPos.transform.rotation);
                    transform.position = posStartFrog;
                    EndZones.finalFive.IsFull = true;
                    array = 0;
                    Timer = 30;
                }
                else
                {
                    array -= 1;
                    transform.position = new Vector3(transform.position.x, posY[array], transform.position.z);
                }
            }

        }
    }

    void OnGUI()
    {
        GUI.Label(new Rect(10, Screen.height - 50, Screen.width - 20, 200), "Timer " + Timer.ToString("0"), TStyle);
    }
}
