Nom du jeu: Frogger

Inspiration: Frogger

R�alis� avec: Unity / C#

R�alis� par: Jean-S�bastien "Zul" Perrier (perrie_b)

D�placement dans les menus: Souris

But du jeu:

Amener votre grenouille dans les carr�s bleus en haut de l'�cran sans heurter une voiture ou tomber dans l'eau.

Comment Jouer:

Fl�che Gauche: 		D�placer la grenouille vers la gauche
Fl�che Droite: 		D�placer la grenouille vers la droite
Fl�che Haut:		D�placer la grenouille vers le haut
Fl�che Bas:		D�placer la grenouille vers le bas
Echap:			Ouvrir le menu de pause